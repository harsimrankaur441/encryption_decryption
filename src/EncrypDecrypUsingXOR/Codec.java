package EncrypDecrypUsingXOR;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Codec {
   
    private static int[] charToIntArray(char ch){
        int i = ch;
        int[] integers = new int[7]; 

        String bin = Integer.toBinaryString(i);
        
        //System.out.println(bin);
        for(int p =0;p < bin.length();p++){
           integers[p] = bin.charAt(p) - '0';
           if(integers[p] > 127){
               throw new IllegalArgumentException("Invalid unicode");
           }
        }
        
      return integers;
    }
    private static char intArrayToChar(int[] bitString){
        
        int len = bitString.length;
        double value =0 ;
        for (int i=0;i < bitString.length ; i++,len--){
            value = value + bitString[i] * Math.pow(2,len-1);
        }
        char c = (char)(int)value;
        return c;
    }  
    
    public static int[] encode(String str){
        
        if(str.length()==0){
            throw new IllegalArgumentException("Null value");

        }
        List list = new ArrayList(Arrays.asList());   
          int[] result = new int[7*str.length()];

        for(int p =0;p < str.length();p++){
           char c = str.charAt(p);
           int a[] =charToIntArray(c);
           
           for(int x=0;x<7;x++){
               result[(p*7)+x] = a[x];
           }
        }
        for (int i =0; i < result.length; i++) {
            //System.out.println(".."+result[i]);
        }
  
        return result;
    }

    
    public static String decode(int[] bits){
      StringBuilder sb = new StringBuilder();

        if(bits.length ==0){
            throw new IllegalArgumentException("Null value");

        }
        if(bits.length/7 ==0){
            throw new IllegalArgumentException("bits array size is not multiple of 7");

        }
        int b[] = new int[7];
        for(int p=0;p < bits.length;p=p+7){
                  
            for (int x=0;x<7;x++){
                b[x] = bits[x+p];
            }
                  char c = intArrayToChar(b);
                  sb.append(c);  
        }

        String str = sb.toString();
        return str;
        
    }
   
    public static void main(String[] args) {
        
        Codec codec = new Codec();
        int arr[] =codec.charToIntArray('C');
        char c = codec.intArrayToChar(arr);
        int arr2[] = codec.encode("Simran");
        for(int i=0;i<arr2.length;i++){
            System.out.print(arr2[i]);
        }
        System.out.println("After decoding -"+codec.decode(arr2));
        
        }
    
}
