
package EncrypDecrypUsingXOR;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;

public class LSFR {
    String seed;
    int tapPosition;
    ArrayList<Character> register = new ArrayList<Character>(); 
     
    public LSFR(String seed,int tapPosition){
        this.seed = seed;
        this.tapPosition = tapPosition;
        if(seed.isEmpty()){
            throw new IllegalArgumentException("Please enter seed value");
        }
        if(tapPosition < 0 || tapPosition > seed.length()){
            throw new IllegalArgumentException("Please enter valid tap position");

        }
        for(int i =0;i < seed.length();i++){
           this.register.add(seed.charAt(i));
        }
           //System.out.println("..."+register.size());
   
    }
    
    public String toString(){
        StringBuffer result = new StringBuffer();
      
      for (char s : this.register) {
         result.append(s);
      }
      return result.toString();
    }
    
    public LSFR(int seedLength,int tapPosition){
        if(seedLength <=0 ){
            throw new IllegalArgumentException("Invalid seed length");
        }
        if(tapPosition <=0 || tapPosition > seedLength){
            throw new IllegalArgumentException("Invalid tap position value");
        }
        this.seed = new Random().ints(seedLength, 0, 2).mapToObj(Integer::toString).collect(Collectors.joining());
        this.tapPosition = tapPosition;
        for(int i =0;i < seed.length();i++){
           this.register.add(seed.charAt(i));
        }
        //System.out.println("..."+register.size());
    }
     
    public int getTapPosition(){
        return this.tapPosition;
    }
    public int nextBit(){ //Shift operation by 1 and replace last bit to XOR of MSB and tapPosition bit
         int x1 =this.register.get(0) - '0';
         int x2 =this.register.get(this.register.size() - this.tapPosition - 1) - '0';

        this.register.remove(0);
        char result = (char)(x1^x2 + '0');
        this.register.add(result);
        return this.register.get(this.register.size() -1) - '0';
    }
     
   
   public static void main(String[] args) {
           LSFR lfsr = new LSFR("01101000010",8);
           for (int i = 0; i < 10; i++) {
                int bit = lfsr.nextBit();
                System.out.println(lfsr.toString() + " " );
            }
        }

    }
        