
package EncrypDecrypUsingXOR;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
 
public class GetTwoDimensionalPixelArray {
       private static int[][] convertToArrayLocation(BufferedImage inputImage) {
 
              final byte[] pixels = ((DataBufferByte) inputImage.getRaster()
                           .getDataBuffer()).getData(); // get pixel value as single array from buffered Image
              final int width = inputImage.getWidth(); //get image width value
              final int height = inputImage.getHeight(); //get image height value
              int[][] result = new int[height][width]; //Initialize the array with height and width
 
              //this loop allocates pixels value to two dimensional array
              for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel++) {
                     int argb = 0;
                     argb = (int) pixels[pixel];
 
                     if (argb < 0) { //if pixel value is negative, change to positive //still weird to me
                           argb += 256;
                     }
 
                     result[row][col] = argb;
                     col++;
                     if (col == width) {
                           col = 0;
                           row++;
                     }
              }
              return result; //return the result as two dimensional array  
    
}
       public static void main(String[] args) throws IOException { //required to throws exception for loading image
 
    BufferedImage inputImage = ImageIO.read(new File("image.jpg")); //load the image from this current folder
              int[][] result = convertToArrayLocation(inputImage); //pass buffered image to the method and get back the result
              //System.out.println("The pixel value of location " + "[19][19] is "+ result[19][19]);
              System.out.println(result);
       } 
      
}
