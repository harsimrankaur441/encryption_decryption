package EncrypDecrypUsingXOR;
import java.util.*;

public class Driver {
      public static void main(String[] args) {
          
          //Take the input string from user  
          Scanner sc= new Scanner(System.in); //System.in is a standard input stream  
          System.out.print("Enter your password : ");  
          String str= sc.nextLine();              //reads string   
          System.out.println("You have entered : "+str);        
          
          //Generation of seed (each character to 7 bits of binary)
          int len = str.length();
          LSFR r1 = new LSFR(len*7, 2); //random generation of seed of given length(or send string of your own seed)
          r1.nextBit(); //modify seed using shift operation
          String seed = r1.toString(); //ArayList to String Conversion
          System.out.println("Seed Value - "+seed);
          
          int[] seed_arr = new int[seed.length()];
          for(int i=0 ; i<seed.length();i++){
              seed_arr[i] = Character.getNumericValue(seed.charAt(i));      //String to array conversion
          }
          Codec c1 = new Codec();
          
          System.out.print("Message in binary- ");
          int encoded_arr[] = c1.encode(str); //coversion of string to its binary array
          for(int i=0;i<encoded_arr.length;i++){
                        System.out.print(encoded_arr[i]);
            }
          
          //XOR operation 
          int[] cipher_arr = new int[seed.length()];
         

          System.out.print("\nCipher in binary- ");

          for(int i=0;i<seed_arr.length;i++){
              cipher_arr[i] = seed_arr[i] ^ encoded_arr[i];     //XORing of seed and original msg which is in binary format in encoded_arr
                System.out.print(cipher_arr[i]);
          }
         
         
          System.out.print("\nDecipher in binary- ");

          int[] decryp_arr = new int[seed.length()];

          for(int i=0;i<seed_arr.length;i++){
              decryp_arr[i] = seed_arr[i] ^ cipher_arr[i];   //XORing of seed and ciphered msg which is in binary format in cipher_arr
              System.out.print(decryp_arr[i]);
          }
          
          System.out.println("\nAfter Decryption : "+c1.decode(decryp_arr)); // decoding decryp_arr binary array to string 
       }
}
